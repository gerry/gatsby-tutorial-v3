import React from 'react'
import Layout from '../components/Layout'
import { StaticImage } from 'gatsby-plugin-image'
import { Link, graphql } from 'gatsby'
import RecipesList from '../components/RecipesList'
import SEO from '../components/SEO'

const About = ({ data:{allContentfulRecipe:{nodes:recipes}} }) => {
  return (
    <Layout>
      <SEO title="About"/>
      <main className="page">
        <section className="about-page">
          <article>
            <h2>I'm baby coloring book poke taxidermy</h2>
            <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.</p>
            <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search</p>
            <Link to="/contact" className="btn">
              contact
            </Link>
          </article>
          <StaticImage src="../assets/images/about.jpeg" 
            className="about-img"
            placeholder="blurred"
            alt="Person Pouring Salt in Bowl" />
        </section>
        <section className="featured-recipes">
          <h5>Look at this Awesomesouce!</h5>
          <RecipesList recipes={recipes} />
        </section>
      </main>
    </Layout>
  )
}

export const query = graphql`
  {
    allContentfulRecipe(
      sort: {fields: title, order: ASC}
      filter: {featured: {eq: true}}
    ) {
      nodes {
        id
        title
        cookTime
        prepTime
        image {
          gatsbyImageData(layout: CONSTRAINED, placeholder: BLURRED)
        }
      }
    }
  }
`
export default About
