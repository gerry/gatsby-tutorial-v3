import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'

const getDataLayout = graphql`
  query FirstQuery {
    site {
      info: siteMetadata {
        author
        description
        title
        complexData {
          age
          name
        }
        person {
          age
          name
        }
      }
    }
  }
`

const FetchData = () => {
  // const data = useStaticQuery(getData);
  // destructuring
  const {
    site: {
      info: { title },
    },
  } = useStaticQuery(getDataLayout);
  return (
    <div>
      {/* <h2>Name: { data.site.info.person.name }</h2> */}
      <h2>site title is : {title}</h2>
    </div>
  )
}

export default FetchData
